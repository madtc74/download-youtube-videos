# Download YouTube Videos

## Description
This program will show you how to download thumbnails, videos, and playlist using Python's pytube package. This could be useful if you want to watch your favorite YouTube videos and playlist offline. It could also be used to extract videos and upload them with your commentary. 

## Visuals
Youtube: https://youtu.be/i5ElHZKK1V8
